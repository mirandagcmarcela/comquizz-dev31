from json import JSONEncoder


class Quiz:
    def __init__(self, title, questions):
        self.title = title
        self.questions = questions


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

    def save(self, quiz):
        json = MyEncoder().encode(quiz)
        f = open("myform.json", "w")
        f.write(json)
        f.close()
